#!/bin/bash
# All functions needed for setting up a container used in the pipeline.
set -euo pipefail

# Ensure home directory is set.
if [ -z "${HOME:-}" ]; then
    export HOME=/tmp
fi
echo "🏠 Home directory is set: ${HOME}"

# Use the shared functions in this script.
source ${PIPELINE_DEFINITION_DIR}/scripts/functions.sh

# Deploy cookies. If cookies weren't provided via $cookies, we set up an empty
# file.
# NOTE(mhayden): Set a default temporarily until we work out how to use the
# prepared pipeline definition code for all stages of a pipeline.
export COOKIE_FILE=${COOKIE_FILE:-/tmp/.cookies}
if [[ -n "${cookies:-}" ]]; then
    echo_green "🍪 Cookies found. Deploying to: ${COOKIE_FILE}"
    base64 -d <<< $cookies > $COOKIE_FILE
else
    echo_yellow "🍪 No cookies found. Creating empty cookie file."
    echo "# HTTP Cookie File" > $COOKIE_FILE
fi

# Configure curl
cat >~/.curlrc <<EOF
    --location
    --silent
    --show-error
    --retry 5
    --connect-timeout 30
    --cookie $COOKIE_FILE
EOF

# Ensure the workdir and artifacts directory exist
mkdir -p "${WORKDIR}" "${ARTIFACTS_DIR}"

# Ensure all of the required software is packaged.
source ${PIPELINE_DEFINITION_DIR}/scripts/prepare_software.sh

# Get CPU count related variables for compiling kernels, running xz etc.
eval "$(get_cpu_count)"

echo_green "💻 Found ${CPUS_AVAILABLE} CPUs, setting job count to ${MAKE_JOBS}".
