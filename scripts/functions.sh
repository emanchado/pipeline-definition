#!/bin/bash
# Add any bash functions needed in the pipeline to this file.
set -euo pipefail

# Write a bold green message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in green
function echo_green {
    echo -e "\e[1;32m${1}\e[0m"
}

# Write a bold red message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in red
function echo_red {
    echo -e "\e[1;31m${1}\e[0m"
}

# Write a bold yellow message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in red
function echo_yellow {
    echo -e "\e[1;33m${1}\e[0m"
}

# Use this to execute an application and log the invocation by prepending
# this to an existing command.
function run_logged() {
    bash -x -c '"$@"' -- "$@"
}

# Run kpet's print-test-cases with default parameters, e.g. the cookies file
function kpet_print_test_cases() {
    run_logged ${DEF_VENV_BIN}/kpet --db kpet-db run print-test-cases --cookies ${COOKIE_FILE} "$@"
}

# Generate -v key=value arguments for kpet from the trigger variables given
# in KPET_VARIABLES in the kpet_variable_arguments array.
function kpet_generate_variable_arguments() {
    kpet_variable_arguments=()
    for kpet_variable in $KPET_VARIABLES; do
        if [ -v "$kpet_variable" ]; then
            kpet_variable_arguments+=("-v" "${kpet_variable}=${!kpet_variable}")
        fi
    done
}

# Run kpet's print-test-cases for SPECIFIC_TREE, ARCH_CONFIG, patch_urls and
# test set = $1, to see if there are any test cases for this test set and
# return 0 if so, return 1 if there's no match.
function kpet_verify_test_group4patches() {
    TEST_SET=$1
    RC=0
    if [ -z "${patch_urls:-}" ]; then
    # Fail the test if there are no patches
    return 1
    fi
    RESULT=`kpet_print_test_cases -s "$TEST_SET" -t ${SPECIFIC_TREE} -a ${ARCH_CONFIG} ${patch_urls} || RC=$?`
    if [[ $RC -eq 0 && -n "$RESULT" ]]; then
    return 0
    else
    return 1
    fi
}

# Create beaker TEMPLATE_FILE for GROUP_NAME of SET_NAMES with PATCH_ARG,
# write stderr to DEST_STDERR.
function generate_kpet_test_group() {
    TEMPLATE_FILE=$1
    GROUP_NAME=$2
    SET_NAMES=$3
    PATCH_ARG=$4
    DEST_STDERR=$5

    run_logged ${DEF_VENV_BIN}/kpet --db kpet-db run generate \
    -v "job_group=$GROUP_NAME" \
    "${kpet_variable_arguments[@]}" \
    ${TESTS_BEAKER_ZIP_URL:+-v "suites_zip_url=$TESTS_BEAKER_ZIP_URL"} \
    -t ${SPECIFIC_TREE} \
    -k "$KERNEL_URL" -a ${ARCH_CONFIG} -s "$SET_NAMES" \
    -c "$COMPONENTS_RE" \
    -d "cki@gitlab:${CI_PIPELINE_ID} "'##KVER##'"@${CI_COMMIT_REF_NAME} ${ARCH_CONFIG}" \
    -o "$TEMPLATE_FILE" --cookies ${COOKIE_FILE} $PATCH_ARG 2> $DEST_STDERR
}

# Set an rc parameter.
# Args: section name value
function rc_set() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2" "$3"
}

# Get an rc parameter.
# Args: section name
# Output: value
function rc_get() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2"
}

# Delete an rc parameter.
# Args: section name
function rc_del() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2"
}

# Set an rc state parameter.
# Args: name value
function rc_state_set() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2"
}

# Get an rc state parameter.
# Args: name
# Output: value
function rc_state_get() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1"
}

# Delete an rc state parameter.
# Args: name
function rc_state_del() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1"
}

# Functions for calculating build time durations
#
# Store time before build start
function store_time() {
    BUILD_TIME_START=$(date +"%s")
}

# Calculate build time and store into rc
# Args: rc_variable
function calculate_duration() {
    rc_state_set "$1" $(($(date +"%s") - BUILD_TIME_START))
}

# Join an array using a delimiter.
function join_by() {
    local IFS="$1"
    shift
    echo "$*"
}

# Join an array with a multi-character delimiter, but ensure that
# delimiter is added to the first item in the array as well. This is
# helpful for adding '--with=' and '--without' to rpmbuild commands.
function join_by_multi() {
    local d=$1
    shift
    printf "%s" "${@/#/$d}"
}

# Build pip wheels and a git archive for each project.
# Args:
#   $1: Base project name (such as 'skt' or 'kpet')
function make_archive_and_wheels {
    pushd ${GIT_BASE}/${1}
        git archive --format=tar -o ${REPO_DIR}/${1}.tar HEAD
        # Build wheels if this is a python project.
        if [[ -f requirements.txt ]] || [[ -f setup.py ]]; then
            echo_green "Building wheels for ${1}"
            # Build a wheel for the base package.
            python3 -m pip wheel --quiet --wheel-dir ${WHEELHOUSE_DIR} .
            # Build dependencies if a requirements.txt file is present.
            if [[ -f requirements.txt ]]; then
                python3 -m pip wheel --quiet --wheel-dir ${WHEELHOUSE_DIR} -r requirements.txt
            fi
        fi
    popd
}

# Clone a repository from the git-cache.
# Args:
#   $1: complete repository URL
#   $@: additional arguments for git clone, e.g. working directory
function git_cache_clone {
    local owner_repo="$(get_owner_repo "$1")"
    rm -rf "${GIT_CACHE_DIR}/${owner_repo}"
    mkdir -p "${GIT_CACHE_DIR}/${owner_repo}"
    aws_s3_download BUCKET_GIT_CACHE "${owner_repo}.tar" - | \
        tar -xf - -C "${GIT_CACHE_DIR}/${owner_repo}"
    shift
    git clone --quiet "${GIT_CACHE_DIR}/${owner_repo}" "$@"
    rm -rf "${GIT_CACHE_DIR}/${owner_repo}"
}

# Attempt to clone from the upstream source first. If that fails, fall back
# to our cached clones that are updated every 15 minutes.
# Args:
#   $1: repo's HTTPS URL (without 'https://')
#   $2: directory for cloned repository
function git_clone {
    echo_green "Cloning $1 from upstream"
    if ! git clone --quiet "https://${1}" $2; then
        echo_yellow "Cloning from upstream failed, attempting to clone from cache"
        git_cache_clone "https://${1}" "$2"
    fi
}

# Run the reliable git cloner for repos on github.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
function git_clone_github {
    git_clone "github.com/CKI-project/${1}.git" $2
}

# Run the reliable git cloner for repos on gitlab.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
function git_clone_gitlab_com {
    git_clone "gitlab.com/cki-project/${1}.git" $2
}

# Run the reliable git cloner for repos on gitlab.cee.redhat.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
function git_clone_gitlab_cee {
    git_clone "gitlab.cee.redhat.com/cki-project/${1}.git" $2
}

# Get the short tag for a commit in a repository.
# Args:
#   $1: path to the git repository
function get_short_tag {

    # Does the repo directory exist?
    if [[ ! -d $1 ]]; then
        exit 1
    fi

    # Switch to the repo directory.
    pushd $1 > /dev/null

    # Describe the most recent commit.
    TAG=$(git describe --abbrev=0 || true)

    if [[ $TAG =~ ^kernel ]]; then
        # Handle kernel tags in kernel-VERSION-RELEASE format.
        SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 3-)
    elif [[ $TAG =~ ^[0-9] ]]; then
        # Handle kernel tags in VERSION-RELEASE format.
        SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 2-)
    else
        # Handle upstream kernel tags, such as v5.0-rc8, or situations where
        # there are no tags at all.
        SHORT_TAG=$(git rev-list --max-count=1 HEAD | cut -b-7)
    fi

    # Return the short tag we generated with a dash prepended.
    echo "-${SHORT_TAG}"

    popd > /dev/null
}

# Get the owner.repo name that is used in the git-cache for a repository.
# Args:
#   $1: url to the git repository
function get_owner_repo {
    GIT_URL="$1"
    GIT_URL_LC="${GIT_URL,,}"                 # everything lowercase
    GIT_URL_PATH="${GIT_URL_LC#*//*/}"        # strip protocol and host
    GIT_URL_PATH="${GIT_URL_PATH%/}"          # strip trailing slashes
    GIT_URL_PATH="${GIT_URL_PATH%.git}"       # strip optional .git suffix
    GIT_URL_REPO="${GIT_URL_PATH##*/}"        # strip any directories
    if [[ "${GIT_URL_PATH}" == */* ]]; then
        GIT_URL_OWNER="${GIT_URL_PATH%/*}"    # strip repo
        GIT_URL_OWNER="${GIT_URL_OWNER##*/}"  # strip any parent directories
    else
        # default to "git" owner
        # this makes the following URLs compatible (cgit):
        # - git://host.com/repo.git
        # - http://host.com/git/repo.git
        GIT_URL_OWNER="git"
    fi
    echo "${GIT_URL_OWNER}.${GIT_URL_REPO}"
}

# Determine the total number of CPUs available.
#
# NOTE(mhayden): This process must be done carefully since `nproc` in a
# container may say that 64 CPUs are available, but cgroups have limited the
# CPU count to 4. Tools like make, rpm, and xz are not able to determine
# cgroup limits, so they will use the CPU count from the system instead. This
# causes reduced performance and often causes OOM errors.
function get_cfs_quota {
    local QUOTA_FILE="/sys/fs/cgroup/cpu/cpu.cfs_quota_us"
    if [ -f "${QUOTA_FILE}" ]; then
        cat "${QUOTA_FILE}"
    fi
}

function get_cpu_count {
    local CFS_QUOTA="$(get_cfs_quota)"
    local CPUS_AVAILABLE MAKE_JOBS RPM_BUILD_NCPUS
    if [ -n "${CFS_QUOTA}" ] && [ "${CFS_QUOTA}" != "-1" ]; then
        CPUS_AVAILABLE="$((CFS_QUOTA / 100 / 1000))"
    else
        CPUS_AVAILABLE="$(nproc)"
    fi

    # On the off chance that the CPU count is less than 1, set the count to 1.
    if [ "${CPUS_AVAILABLE}" -lt 1 ]; then
        CPUS_AVAILABLE=1
    fi

    # Set the number of make jobs based on kernel developer recommendations.
    export MAKE_JOBS="$((CPUS_AVAILABLE * 3 / 2))"

    # RPM needs a special environment variable set so that this command returns
    # the correct number of CPUs in the container: rpm --eval %{_smp_mflags} If
    # this is not set, too many XZ_THREADS will be spawned and we will get OOM
    # errors. See FASTMOVING-1526.
    export RPM_BUILD_NCPUS="${MAKE_JOBS}"

    echo 'export CPUS_AVAILABLE="'"${CPUS_AVAILABLE}"'"'
    echo 'export MAKE_JOBS="'"${MAKE_JOBS}"'"'
    echo 'export RPM_BUILD_NCPUS="'"${RPM_BUILD_NCPUS}"'"'
}

eval "$(sed -n '/AWS-BEGIN/,/AWS-END/p' "${BASH_SOURCE[0]%/*}/../cki_pipeline.yml")"
