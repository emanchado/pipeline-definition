#!/bin/bash
# Add any bash functions needed in tests to this file.
set -euo pipefail

function _failed_init {
    # keep track of number of failed tests
    export FAILED_FILE=$(mktemp)
    trap 'rm -rf "${FAILED_FILE}"' EXIT
    echo 0 > "${FAILED_FILE}"
}

function _failed_check {
    # fail the script if any tests failed.
    echo
    FAILED="$(cat "${FAILED_FILE}")"
    if [ "${FAILED}" -gt 0 ]; then
        echo_red "$FAILED tests failed."
        exit 1
    fi
    echo_green "All tests passed."
}

function _check_equal {
    echo
    echo_yellow "$4?"
    echo "  Observed: $3 == $1"
    echo "  Expected: $3 == $2"
    if [ "$1" == "$2" ]; then
        echo_green "  Result: PASS"
    else
        echo_red "  Result: FAIL"
        FAILED="$(cat "${FAILED_FILE}")"
        echo "$(("${FAILED}" + 1))" > "${FAILED_FILE}"
    fi
}
