#!/bin/bash
set -euo pipefail

echo '
    ____                 __  _
   / __/_  ______  _____/ /_(_)___  ____  _____
  / /_/ / / / __ \/ ___/ __/ / __ \/ __ \/ ___/
 / __/ /_/ / / / / /__/ /_/ / /_/ / / / (__  )
/_/  \__,_/_/ /_/\___/\__/_/\____/_/ /_/____/

'

source scripts/functions.sh
source tests/helpers.sh

_failed_init

function check_owner_repo {
    declare -A OWNER_REPOS

    OWNER_REPOS[http://git-extensions-are-stripped/a/b/c.git]=b.c
    OWNER_REPOS[http://extension-and-slashes-are-stripped/a/b/c.git/]=b.c
    OWNER_REPOS[http://slashes-are-stripped/a/b/c/]=b.c
    OWNER_REPOS[http://two-directories/a/b/c]=b.c
    OWNER_REPOS[http://one-directory/a/b]=a.b
    OWNER_REPOS[http://no-directory/a]=git.a
    OWNER_REPOS[http://UPPER-CASE/A/B/C]=b.c

    OWNER_REPOS[git://host.com/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[http://host.com/git/name1-1.1.a.git]=git.name1-1.1.a
    OWNER_REPOS[git://host.com/name2.git]=git.name2
    OWNER_REPOS[http://host.com/git/name2.git]=git.name2
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git]=arm64.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/davem/net-next.git]=davem.net-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/jejb/scsi.git]=jejb.scsi
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git]=next.linux-next
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/powerpc/linux.git]=powerpc.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rdma/rdma.git]=rdma.rdma
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-rt-devel.git]=rt.linux-rt-devel
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/sashal/linux-stable.git]=sashal.linux-stable
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git]=stable.linux
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git]=stable.linux-stable-rc
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/stable/stable-queue.git]=stable.stable-queue
    OWNER_REPOS[https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git]=torvalds.linux
    OWNER_REPOS[https://gitlab.com/CKI-project/kernel-ark.git]=cki-project.kernel-ark
    OWNER_REPOS[https://host.com/gerrit/kernel-name3]=gerrit.kernel-name3
    OWNER_REPOS[https://host.com/gerrit/kernel-name4.git]=gerrit.kernel-name4

    for URL in "${!OWNER_REPOS[@]}"; do
        _check_equal "$(get_owner_repo "${URL}")" "${OWNER_REPOS[${URL}]}" "owner.repo" "Is the owner.repo correct for $URL"
    done
}
check_owner_repo

# function() instead of function{} so that the stub functions are scoped
function check_git_cache_clone()
(
    declare aws_s3_download_params
    function git {
        /usr/bin/git "$@" 2>&1 > /dev/null
        echo "git_params='$@'" >&2
    }
    function aws_s3_download {
        mkdir -p test-output/git-repo
        /usr/bin/git init test-output/git-repo 2>&1 > /dev/null
        tar -C test-output/git-repo -cf - .
        echo "aws_s3_download_params='$@'" >&2
    }
    trap 'rm -rf test-output/' EXIT
    local GIT_CACHE_DIR="test-output/git-cache"
    eval "$(git_cache_clone https://host.com/a/b/repo.git tests/workdir --quiet 2>&1 >/dev/null)"
    git_params_array=($git_params)
    aws_s3_download_params_array=($aws_s3_download_params)

    _check_equal "${aws_s3_download_params_array[0]}" "BUCKET_GIT_CACHE" "bucket" "Does git_cache_clone pass the bucket correctly into aws_s3_download"
    _check_equal "${aws_s3_download_params_array[1]}" "b.repo.tar" "tarfile" "Does git_cache_clone pass the tar file name correctly into aws_s3_download"
    _check_equal "${git_params_array[-1]}" "--quiet" "param" "Does git_cache_clone pass the additional parameters to git clone"
    _check_equal "$(stat test-output/git-repo/.git/config > /dev/null && echo yes || echo no)" "yes" ".git/config exists" "Does the git repo get successfully cloned"
)
check_git_cache_clone

# function() instead of function{} so that the stub functions are scoped
function check_cpu_count()
(
    local NPROC_RESULT="$1"
    local CFS_QUOTA_RESULT="$2"
    local EXPECTED_CPUS_AVAILABLE="$3"
    local EXPECTED_MAKE_JOBS="$4"
    local EXPECTED_RPM_BUILD_NCPUS="$5"
    local MESSAGE="$6"
    function nproc {
        echo "${NPROC_RESULT}"
    }
    function get_cfs_quota {
        echo "${CFS_QUOTA_RESULT}"
    }
    eval "$(get_cpu_count)"

    _check_equal "${CPUS_AVAILABLE}" "${EXPECTED_CPUS_AVAILABLE}" cpu_count "Is the CPU count correct with ${MESSAGE}"
    _check_equal "${MAKE_JOBS}" "${EXPECTED_MAKE_JOBS}" make_jobs "Is the make job count correct with ${MESSAGE}"
    _check_equal "${RPM_BUILD_NCPUS}" "${EXPECTED_RPM_BUILD_NCPUS}" rpm_build_ncpus "Is the RPM build CPU count correct with ${MESSAGE}"
)
check_cpu_count 1000 800000 8 12 12 "a cgroup limit"
check_cpu_count 1000 50000 1 1 1 "a cgroup limit for less than 1 cpu"
check_cpu_count 1000 "-1" 1000 1500 1500 "an unbounded cgroup limit"
check_cpu_count 1000 "" 1000 1500 1500 "no cgroup limit file"

function check_kpet_variable_arguments() {
    local MESSAGE="$1"
    local EXPECTED_COUNT="$2"
    shift 2
    local KPET_VARIABLES=''
    for variable_item in "$@"; do
        variable_key="${variable_item%%=*}"
        variable_value="${variable_item#*=}"
        KPET_VARIABLES="${KPET_VARIABLES} ${variable_key}"
        if [ -n "${variable_value}" ]; then
            local "${variable_item}"
        fi
    done
    kpet_generate_variable_arguments
    _check_equal "${#kpet_variable_arguments[@]}" "$((2 * EXPECTED_COUNT))" "argument count" "Is the number of arguments correct with ${MESSAGE}"
    local -i index=0
    while [ "${index}" -lt "${#kpet_variable_arguments[@]}" ]; do
        _check_equal "${kpet_variable_arguments[${index}]}" "-v" "argument" "Is argument $((index + 1)) as expected with ${MESSAGE}"
        index+=1
        _check_equal "${kpet_variable_arguments[${index}]}" "variable$((index / 2 + 1))_key=value $((index / 2 + 1))" "argument" "Is argument $((index + 1)) as expected with ${MESSAGE}"
        index+=1
    done
}
check_kpet_variable_arguments "no variables" 0
check_kpet_variable_arguments "a non-existent variable" 0 "variable1_key="
check_kpet_variable_arguments "a variable with spaces" 1 "variable1_key=value 1"
check_kpet_variable_arguments "multiple variables" 2 "variable1_key=value 1" "variable2_key=value 2"

_failed_check
