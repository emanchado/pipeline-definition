#!/usr/bin/env python3
# Copyright (c) 2019 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General
# Public License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Script using koji to download kernel-headers for kernel."""
import os
import pprint
import re
import shlex
import subprocess
import sys
import time

from cki_lib.misc import safe_popen, init_logger

LOGGER = init_logger(__name__)


class KojiWrap:
    """Wrap koji utility to download taskids and correlate info from webui."""

    def __init__(self, web_url, server_url, top_url):
        self.web_url = web_url
        self.server_url = server_url
        self.top_url = top_url

    def run_cmd(self, cmd):
        """Run shell command 'koji' with server params set. """
        cmd = f"""koji --server {self.server_url} """ + \
              f"""--weburl {self.web_url} --topurl {self.top_url} {cmd}"""

        stdout, stderr, returncode = safe_popen(shlex.split(cmd),
                                                stdout=subprocess.PIPE,
                                                stderr=subprocess.PIPE)

        return stdout, stderr, returncode

    def list_builds(self, **kwargs):
        """Run 'koji list-builds' and pass arguments (--key=value) to it."""
        cmd = 'list-builds --reverse'
        for key, value in kwargs.items():
            cmd += f' --{key}={value}'

        stdout, _, returncode = self.run_cmd(cmd)
        if returncode:
            raise RuntimeError(f'error: koji retcode=={returncode}')

        return stdout

    def koji_correlate_headers(self, nvr, exact=True):
        """Get task info for a given kernel taskid, find kernel-headers build.

        Arguments:
            nvr:                str, the kernel-headers build to download
            exact:              bool, take only exact headers version
        Returns:
            a list of build nvrs (str) for kernel-headers or []
        """
        # pull specific build/owner to help us look for a related koji package
        nvr = nvr.replace('kernel-', '')
        fed_rev = re.fullmatch(r'.*?\.(fc[0-9]+).*?', nvr).group(1)

        # find kernel-headers with nvr exactly matching kernel, or those that
        # were built before kernel
        alt_regex = fr'^(kernel-headers-.*?\.{fed_rev})\s+([^\n]+)\s+([^\n]+)'
        regex = fr'^(kernel-headers-{re.escape(nvr)})\s+([^\n]+)\s+([^\n]+)' \
            if exact else alt_regex

        stdout = self.list_builds(state='COMPLETE', package='kernel-headers')
        results = []
        for line in stdout.splitlines():
            match_result = re.match(regex, line)
            if match_result:
                results.append(match_result.group(1))

        return results

    def taskinfo(self, taskid, desired_fields):
        """Get info about a single taskid using 'koji taskinfo'.

        Arguments:
            taskid:         int, taskid to get info about
            desired_fields: list, fields to return, possible fields are:
                            ['ID', 'State', 'Build', 'Created', 'Owner']
        Returns:
            a dict with keys of applicable desired_fields, e.g:
            {'Build': 'kernel-5.2.14-200.fc30',
            'Created': 'Tue, 01 Sep 2010 11:11:11 UTC',
            'ID': '12345678', 'Owner': 'jouser', 'State': 'closed'}
        """
        stdout, _, _ = self.run_cmd(f'taskinfo {taskid}')
        result = {field: None for field in desired_fields}

        for line in stdout.splitlines():
            for field in desired_fields:
                if line.startswith(field):
                    result[field] = line.split(': ')[1]

        if 'Build' in desired_fields:
            result['Build'] = result['Build'].split(' ')[0]

        return result

    def download_build(self, nvr, max_retries=20, **kwargs):
        """Download build (by NVR) using 'koji download-build', allow retries.

        Try to download koji build, sleep 15s on failure, allow at most
        max_retries.

        Arguments:
            nvr:     str, nvr of the build to download
            max_retries: int, how many times to retry on failure, default: 20

            kwargs:
                arch: str, e.g. 'x86_64', the arch to download task for
        """
        LOGGER.info("📥 Downloading packages from Koji/Brew...")
        # prepare failure message
        fail_msg = f"Giving up after {max_retries} tries"

        cmd = 'download-build --noprogress '
        for key, value in kwargs.items():
            cmd += f'--{key}={value} '

        cmd += '--arch=src ' + nvr

        success = False
        while max_retries:
            stdout, stderr, returncode = self.run_cmd(cmd)
            success = returncode == 0
            if not success:
                LOGGER.debug(stdout)
                LOGGER.debug(stderr)
                if 'Forbidden' in stderr:
                    return False

            if success:
                return True

            time.sleep(15)
            max_retries -= 1

        if not success:
            LOGGER.info(fail_msg)

        return False


def go_download(arch, dst_dir, server_url, web_url, top_url, taskid=None,
                nvr=None):
    # pylint: disable=R0913
    """Download kernel-headers related to kernel to dst_dir.

    Arguments:
        arch:       str, e.g. 'x86_64', the arch to download task for
        taskid:     int, taskid to find kernel-headers for
        dst_dir:    str, a directory where to download the files
        server_url: str, koji server_url param
        web_url:    str, koji web_url param
        top_url:    str, koji top_url param
    """
    koji_wrap = KojiWrap(web_url, server_url, top_url)

    # get & parse info about the original koji task (by taskid)
    if taskid:
        taskinfo = koji_wrap.taskinfo(taskid, ['Build'])
        kernel_nvr = taskinfo['Build']
    elif nvr:
        kernel_nvr = nvr
    else:
        raise RuntimeError('you need to input taskid or nvr')

    related_nvrs = koji_wrap.koji_correlate_headers(kernel_nvr, exact=True)
    if not related_nvrs:
        related_nvrs = koji_wrap.koji_correlate_headers(kernel_nvr,
                                                        exact=False)

    if not related_nvrs:
        raise RuntimeError('Failed to find kernel-headers task!')

    start_dir = os.getcwd()
    try:
        # download task to dst_dir
        os.chdir(dst_dir)

        LOGGER.debug(
            f'The list of NVRs we got is: {pprint.pformat(related_nvrs)}')
        for temp_nvr in related_nvrs:
            if koji_wrap.download_build(temp_nvr, arch=arch):
                break
    except:  # noqa: E722
        # in any case, return to directory we started in
        os.chdir(start_dir)
        # re-raise exception
        raise
    else:
        # in any case, return to directory we started in
        os.chdir(start_dir)


def main():
    # pylint: disable=unbalanced-tuple-unpacking
    """Get params and complete go_download."""
    if len(sys.argv) != 4:
        raise RuntimeError('invalid input arguments')

    cki_pipeline_type = os.environ['cki_pipeline_type']

    server_url = os.environ['server_url']
    web_url = os.environ['web_url']
    top_url = os.environ['top_url']

    if cki_pipeline_type == 'copr':
        _, arch, buildid, dst_dir = sys.argv
        nvr = os.environ['nvr']

        go_download(arch, dst_dir, server_url, web_url, top_url, nvr=nvr)

    else:
        _, arch, taskid, dst_dir = sys.argv

        go_download(arch, dst_dir, server_url, web_url, top_url,
                    taskid=taskid)


if __name__ == '__main__':
    main()
